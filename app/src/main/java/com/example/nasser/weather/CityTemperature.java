package com.example.nasser.weather;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class CityTemperature extends Fragment {
    Temperature temperature;

    TemperatureValues temperatureValues;

    public CityTemperature() {
        temperatureValues = TemperatureValues.getInstance();

    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_city_temperature, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        DownloadJsonTask downloadJsonTask = new DownloadJsonTask(listView, getActivity());
        downloadJsonTask.execute("https://goo.gl/etSf5m");
        return view;
    }

    class CustomBaseAdapter extends BaseAdapter {
        public DataModel mDataModel;
        Activity mContext;
        City city;

        public CustomBaseAdapter(Activity context){
            if(context == null){
                Log.e("CustomAdapter: ", "Context is null");
            }else{
                this.mContext = context;
                mDataModel = new DataModel(mContext);

            }
        }

        @Override
        public int getCount(){
            return temperatureValues.getTemperature().size();

        }
        @Override
        public long getItemId(int position){
            return position;
        }

        @Override
        public Temperature getItem(int position){
            return temperatureValues.getTemperature().get(position);
        }

        @Override
        public View getView(final int position, View view, ViewGroup container){
            if(view == null){
                LayoutInflater inflater = (LayoutInflater) mContext.getLayoutInflater();
                view = inflater.inflate(R.layout.list_item, null);
            }
            //Create an instance of temperature class to bind it with widgets
            temperature = new Temperature();
            temperature = getItem(position);
            TextView temperDate = (TextView) view.findViewById(R.id.temperDate);
            TextView temperValue = (TextView) view.findViewById(R.id.temperValue);
            Button btnSave = (Button) view.findViewById(R.id.btnSave);
            temperDate.setText(temperature.getDate());
            temperValue.setText(Double.toString(temperature.getValue()));
            city = new City("Moscow");
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("AddTemp", temperatureValues.getTemperature().get(position).getDate());
                    Log.i("AddTemp", Double.toString(temperatureValues.getTemperature().get(position).getValue()));
                    mDataModel.addTemper(temperatureValues.getTemperature().get(position), city);
                    Toast.makeText(mContext, "Temperature saved", Toast.LENGTH_LONG).show();
                }
            });

            return view;
        }


    }

}
