package com.example.nasser.weather;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataModel {
    private SQLiteDatabase db;
    private static String DB_NAME = "Weather_DB";
    private int DB_VERSION = 1;
    private static String CITY_TABLE = "City";
    private static String TEMPER_TABLE = "Temperature";
    private static String ID_COLUMN = "_id";
    public static String NAME_COLUMN = "name";
    private static String NAME_STATEMENT = " VARCHAR(50) NOT NULL UNIQUE";
    private static String DATE_COLUMN = "date Text NOT NULL, ";
    private static String TEMPER_COLUMN = "temperature";
    private static String PRIMARY_KEY_STATEMENT = " INTEGER PRIMARY KEY AUTOINCREMENT,";


    public DataModel(Context context) {
            CustomSqLiteOpenHelper helper = new CustomSqLiteOpenHelper(context);
            db = helper.getWritableDatabase();
    }


    public Cursor getCities(){
        Cursor c;
        String query = "SELECT * FROM " + CITY_TABLE + ";";
        try{
            c = db.rawQuery(query, null);
            c.moveToFirst();
            return c;
        }catch(SQLException e){
            Log.e("getCities", e.getMessage());
            return null;
        }
    }

    public void addCity (City city){
        Log.i("DataModel-City: ", city.getCityName());
        String query = "INSERT INTO " + CITY_TABLE + "(" + NAME_COLUMN + ") VALUES ('" + city.getCityName() + "');";
        try{
            db.execSQL(query);
            Log.i("DataModel-City: ", "City Added");
        }catch(SQLException e){
            e.printStackTrace();
            Log.e("DataModel-City", e.getMessage());
        }

    }

    public Cursor getTempByDate(String date, String cityName){
        String subQuery = "(SELECT " + ID_COLUMN + " FROM " + CITY_TABLE + " WHERE " + "name='" + cityName + "')";
        try{
            Cursor temperCursor=db.rawQuery("SELECT " + TEMPER_COLUMN + " FROM " + TEMPER_TABLE + " WHERE city=" + subQuery + " AND date LIKE '" +
                    date + "%';", null);
            temperCursor.moveToFirst();
            return temperCursor;
        }catch(SQLException e){
            Log.e("GetTemer", e.getMessage());
            return null;
        }


    }


    public void getCityId(String cityName){
        String query = "SELECT " + ID_COLUMN + " FROM " + CITY_TABLE + " WHERE " + "name='" + cityName + "';";
        try{
            Cursor cityCursor = db.rawQuery(query, null);
            cityCursor.moveToFirst();
            Log.i("CityQuery", query);
            if(!cityCursor.moveToNext()){
                Log.i("CityQuery", "No results foubd");
            }else{
                while(cityCursor.moveToNext()){
                    //Get _id column index
                    int index = cityCursor.getColumnIndex(ID_COLUMN);
                    Log.i("GetCity", Integer.toString(cityCursor.getInt(index)));
                }
            }

        }catch (SQLException e){
            Log.e("GetCity", e.getMessage());
        }


    }


    public void addTemper (Temperature temper, City city){
        String query = "INSERT INTO " + TEMPER_TABLE + " (date, temperature, city) VALUES ('" + temper.getDate() + "'," +
                temper.getValue() + ", " +
                "(SELECT " + ID_COLUMN + " FROM " + CITY_TABLE + " WHERE " + NAME_COLUMN + "='" + city.getCityName() + "'));";
        Log.i("DataModel-Temper", temper.getDate());
        Log.i("DataModel-Temper", Double.toString(temper.getValue()));
        Log.i("DataModel-Temper", city.getCityName());
        try{
            db.execSQL(query);
            Log.i("DataModel-Temper", "Tmper added");
        }catch(SQLException e){
            e.printStackTrace();
        }

        }

        private class CustomSqLiteOpenHelper extends SQLiteOpenHelper {

            public CustomSqLiteOpenHelper(Context context) {
                super(context, DB_NAME, null, DB_VERSION);
            }

            @Override
            public void onCreate(SQLiteDatabase sqLiteDatabase) {
                try{
                    String dropTable = "DROP TABLE " + CITY_TABLE;
                    sqLiteDatabase.execSQL(dropTable);
                    dropTable = "DROP TABLE " + TEMPER_TABLE;
                    sqLiteDatabase.execSQL(dropTable);
                }catch(Exception e){
                    e.printStackTrace();
                }finally {
                    String newTableQueryString = "CREATE TABLE " + CITY_TABLE + " (" + ID_COLUMN + PRIMARY_KEY_STATEMENT + NAME_COLUMN +
                            NAME_STATEMENT + ");";
                    sqLiteDatabase.execSQL(newTableQueryString);

                    newTableQueryString = "CREATE TABLE " + TEMPER_TABLE + " (" + ID_COLUMN + PRIMARY_KEY_STATEMENT + DATE_COLUMN +
                            TEMPER_COLUMN + " DOUBLE NOT NULL, city INTEGER NOT NULL, FOREIGN KEY(city) REFERENCES " +
                            CITY_TABLE + "(" + ID_COLUMN + "));";
                    sqLiteDatabase.execSQL(newTableQueryString);
                }

            }

            @Override
            public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

            }
        }

}
