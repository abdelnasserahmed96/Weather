package com.example.nasser.weather;

public interface FragmentComms {
    void onCityClicked(String cityName);
}
