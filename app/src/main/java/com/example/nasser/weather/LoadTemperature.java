package com.example.nasser.weather;

import android.annotation.TargetApi;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;


public class LoadTemperature extends Fragment{
    DataModel mDataModel;
    String showDate;
    DatePicker dp;
    Cursor c;
    TextView date;
    TextView value;

    @Override
    public void onCreate(Bundle savedInstace){
        super.onCreate(savedInstace);
        mDataModel = new DataModel(getActivity().getApplication());
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedIntance){
        View view = inflater.inflate(R.layout.fragment_load_temperature, container, false);
        date = (TextView) view.findViewById(R.id.date);
        value = (TextView) view.findViewById(R.id.value);
        Button btnSearch = (Button) view.findViewById(R.id.btnSearch);
        Button btnShare = (Button) view.findViewById(R.id.btnShare);
        dp = (DatePicker) view.findViewById(R.id.datePicker);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDate = dp.getYear() + "-" + String.format("%02d",(dp.getMonth() + 1)) + "-" + String.format("%02d",dp.getDayOfMonth());
                Toast.makeText(getActivity(), showDate, Toast.LENGTH_LONG).show();
                //Go to showData function to set TextView and cursor manipulation
                date.setText(showDate);
                //Get the corespponding city
                mDataModel.getCityId("Giza");
                c = mDataModel.getTempByDate(showDate, "Moscow");
                showData(c);

            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                String message = "Temperature for " + showDate + "\n Value is: " + value.getText() + "\n This message is created using Nasser's weather App, please tell me your advice and recommendation";
                sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                Log.i("WhatsApp Message", message);
                sendIntent.setType("text/plain");

                String title = "Choose and App to send the temperature to";

                Intent chooser = Intent.createChooser(sendIntent, title);

                if(sendIntent.resolveActivity(getActivity().getPackageManager()) != null){
                    startActivity(chooser);
                }
            }
        });


        return view;
    }

    public void showData(Cursor c){
        //We entered the function
        Log.i("LoadTemper", "Show data");
        //First check if the cursor has a null value
        if(c == null){
            Log.e("showData", "Cursor is null");
            Toast.makeText(getActivity(), "Error in reading data", Toast.LENGTH_LONG).show();
        }else{
            //Check if there is no result to set a string in TextView value
            if(!c.moveToNext()){
                value.setText("No temperature is saved for this date");
            }else{
                while(c.moveToNext()){
                    //Get the index of temperature column
                    int temperatureIndex = c.getColumnIndex("temperature");
                    //For me to check the cursor
                    Log.i("LoadTemper", "Cursor is not null");
                    Log.i("LoadTemper", Double.toString(c.getDouble(temperatureIndex)));
                    //For the user
                    value.setText(Double.toString(c.getDouble(temperatureIndex)));
                }
            }

        }
    }
}
