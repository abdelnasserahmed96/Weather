package com.example.nasser.weather;

import android.app.Activity;
import android.app.ListFragment;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;




public class ShowCities extends ListFragment{
    FragmentComms fragmentComms;
    public ShowCities() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataModel dm = new DataModel(getActivity().getApplication());
        Cursor cities = dm.getCities();
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_list_item_1, cities,
                new String [] {DataModel.NAME_COLUMN}, new int [] {android.R.id.text1}, 0);
        setListAdapter(cursorAdapter);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        Cursor c = ((SimpleCursorAdapter)l.getAdapter()).getCursor();
        c.moveToPosition(position);
        int cityIndex = c.getColumnIndex("name");
        String clickedCity = c.getString(cityIndex);
        fragmentComms.onCityClicked(clickedCity);
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        fragmentComms = (FragmentComms) activity;

    }

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_show_cities, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        return view;
    }*/



}
