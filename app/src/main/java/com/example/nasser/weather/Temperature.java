package com.example.nasser.weather;

import java.sql.Date;

public class Temperature {
    private String date;
    private double value;

    public Temperature() {
    }

    public Temperature(String date, double value) {

        this.date = date;
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }




}
